package com.io.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class FileInputStreamDemo {

	public static void main(String[] args) {
		File file  = new File("/home/rohit/eclipse-workspace/IOPackage/src/file.txt");
		FileInputStream ipstream = null;
		try {
		    ipstream = new FileInputStream(file);
			int i;
			
			while((i = ipstream.read()) != -1) {
				System.out.println((char)i);
			}
		} catch(FileNotFoundException e) {
			e.printStackTrace();
		}
		catch(IOException e) {
			e.printStackTrace();
		}
		
		finally {
			
			try {
				ipstream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}

}
