package com.io.file;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

public class FileWrite {
	public static void main(String[] args) {
		String fileName="/home/rohit/eclipse-workspace/IOPackage/src/newFile.txt";
		String source = "rest api publish\n" + 
				"ask for review\n" + 
				"\n" + 
				"\n" + 
				"prepare material\n" + 
				"import org.testng.annotations.Test;\n" + 
				"\n" + 
				"import io.restassured.RestAssured;\n" + 
				"import io.restassured.path.json.JsonPath;\n" + 
				"import io.restassured.response.Response;\n" + 
				"\n" + 
				"import static io.restassured.RestAssured.given;\n" + 
				"\n" + 
				"\n" + 
				"//rahulonlinetutor@gmail.com\n" + 
				"//https://mvnrepository.com/artifact/com.github.scribejava/scribejava-core/2.5.3\n" + 
				"\n" + 
				"//https://mvnrepository.com/artifact/com.github.scribejava/scribejava-apis/2.5.3\n" + 
				"\n" + 
				"public class basics9 {\n" + 
				"	\n" + 
				"	\n" + 
				"	}\n" + 
				"	\n" + 
				"	\n" + 
				"}\n" + 
				"\n" + 
				"";
		try {
			FileWriter file = new FileWriter(fileName);
			
			BufferedWriter writer = new BufferedWriter(file);
			
			writer.write(source);
			
			writer.close();
		}
		catch(FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		catch(IOException e) {
			System.out.println(e.getMessage());
		}	
		
	}
}
